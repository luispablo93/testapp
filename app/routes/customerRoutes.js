App.config(function ($routeProvider) {
  $routeProvider
    .when('/home', 
      {
        controller: 'CustomerController',
        templateUrl: 'app/templates/customer.html'
      }
    )
    .otherwise( { redirectTo: '/home' } );
});