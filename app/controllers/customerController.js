App.controller('CustomerController', ['$scope', 
	function ($scope) {
		$scope.showingList 	= 	false;

		$scope.showList = function(){
			$scope.showingList = true;
		};

		$scope.hideList = function(){
			$scope.showingList = false;
		};
	}
]);