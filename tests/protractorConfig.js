// Protactor Configuration protractorConfig.js
exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: 'http://testapp.com/',
  specs: ['spec/pruebaSimpleSpec.js'],
  
  capabilities: {
    'browserName': 'chrome'
  },

  jasmineNodeOpts: {
    showColors: true,
    isVerbose: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 100000,
    print: function() {}
  },

  onPrepare: function () {
    
  }
  
}