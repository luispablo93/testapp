var PruebaSimple = require("./pruebaSimplePageObject.js");

describe('simple angular app test', function() {
	var pruebaSimple = new PruebaSimple();

	beforeAll(function(){
		browser.get("#!/home");
	});

	it('should start on home page', function() {
		browser.sleep(2000);
		expect(browser.getCurrentUrl()).toEqual(browser.baseUrl+'#!/home');
	});

	it("should click bring list button and show list", function(){
		browser.sleep(1000);
		pruebaSimple.showListButton.click();
		browser.sleep(1000);
		expect(pruebaSimple.listContent.isPresent()).toBe(true);
	});

	it("should check list title", function(){
		browser.sleep(1000);
		expect(pruebaSimple.listTitle.getText()).toEqual('Fruits');
	});

	it("should check list subtitle", function(){
		browser.sleep(1000);
		expect(pruebaSimple.listSubtitle.getText()).toEqual('List of fruits:');
	});

	it("should click hide list button and hide list", function(){
		browser.sleep(1000);
		pruebaSimple.hideListButton.click();
		browser.sleep(1000);
		expect(pruebaSimple.listContent.isPresent()).toBe(false);
	});
	
});