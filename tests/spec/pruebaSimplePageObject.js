var pruebaSimple = (function(){
	function pruebaSimple() {
		this.showListButton =	element(by.id('showListButton'));
		this.hideListButton =	element(by.id('hideListButton'));

		this.listTitle		=	element(by.id('listTitle'));
		this.listSubtitle	=	element(by.id('listSubtitle'));
		this.listContent	=	element(by.id('listContent'));
	};

	return pruebaSimple;

})();

module.exports = pruebaSimple;